# OpenML dataset: Dorothea

https://www.openml.org/d/4137

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

DOROTHEA is a drug discovery dataset. Chemical compounds represented by structural molecular features must be classified as active (binding to thrombin) or inactive. This is one of 5 datasets of the NIPS 2003 feature selection challenge.

Source:

a. Original owners 
The dataset with which DOROTHEA was created is one of the KDD (Knowledge Discovery in Data Mining) Cup 2001. The original dataset and papers of the winners of the competition are available at: http://www.cs.wisc.edu/~dpage/kddcup2001/. DuPont Pharmaceuticals graciously provided this data set for the KDD Cup 2001 competition. All publications referring to analysis of this data set should acknowledge DuPont Pharmaceuticals Research Laboratories and KDD Cup 2001. 

b. Donor of database 
This version of the database was prepared for the NIPS 2003 variable and feature selection benchmark by Isabelle Guyon, 955 Creston Road, Berkeley, CA 94708, USA (isabelle '@' clopinet.com). 


Data Set Information:

Drugs are typically small organic molecules that achieve their desired activity by binding to a target site on a receptor. The first step in the discovery of a new drug is usually to identify and isolate the receptor to which it should bind, followed by testing many small molecules for their ability to bind to the target site. This leaves researchers with the task of determining what separates the active (binding) compounds from the inactive (non-binding) ones. Such a determination can then be used in the design of new compounds that not only bind, but also have all the other properties required for a drug (solubility, oral absorption, lack of side effects, appropriate duration of action, toxicity, etc.). 
The original data were modified for the purpose of the feature selection challenge. In particular, we added a number of distractor feature called 'probes' having no predictive power. The order of the features and patterns were randomized. 

DOROTHEA -- Positive ex. -- Negative ex. -- Total 
Training set -- 78 -- 722 -- 800 
Validation set -- 34 -- 316 -- 350 
Test set -- 78 -- 722 -- 800 
All -- 190 -- 1760 -- 1950 

We mapped Active compounds to the target value +1 (positive examples) and Inactive compounds to the target value &ndash;1 (negative examples). 

Number of variables/features/attributes: 
Real: 50000 
Probes: 50000 
Total: 100000

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/4137) of an [OpenML dataset](https://www.openml.org/d/4137). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/4137/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/4137/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/4137/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

